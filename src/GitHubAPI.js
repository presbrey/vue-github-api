(function () {
  function install (Vue, options) {
    // GitHub API base url
    var apiUrl = 'https://api.github.com'

    // GitHub user Private Token or Personal Access Token
    var token = 'you MUST provide an user Private Token or Personal Access Token'

    // reading options
    if (typeof options !== 'undefined') {
      // a token is mandatory to connect to GitHub API,
      // but it could be set later with .setToken
      if (typeof options.token !== 'undefined') {
        token = options.token
      }

      // apiURL is optional for use with GitHub Enterprise,
      if (typeof options.apiURL !== 'undefined') {
        apiUrl = options.apiURL
      }
    }

    // adding an instance method by attaching it to Vue.prototype
    /**
     * A deadly simple Vue.js plugin to consume GitHub API.
     *
     * @mixin
     * @author {@link http://clorichel.com Pierre-Alexandre Clorichel}
     * @license {@link https://gitlab.com/clorichel/vue-github-api/blob/master/LICENSE MIT License}
     * @see {@link https://gitlab.com/clorichel/vue-github-api Project repository on GitLab.com}
     * @see {@link http://clorichel.com http://clorichel.com}
     */
    Vue.prototype.GitHubAPI = {
      /**
       * Set application wide GitHubAPI token value
       * @param {String} newToken The new token value
       * @example
       * // from anywhere in the global Vue scope
       * Vue.GitHubAPI.setToken('your user token')
       *
       * // from a .vue component
       * this.GitHubAPI.setToken('your user token')
       */
      setToken: function (newToken) {
        if (typeof newToken === 'undefined' || newToken == null || newToken === '') {
          console.error('[vue-github-api] You MUST provide a non empty Private Token or Personal Access Token')
          return
        }
        token = newToken
      },

      /**
       * Set application wide GitHubAPI URL
       * @param {String} newURL The new token value
       * @example
       * // from anywhere in the global Vue scope
       * Vue.GitHubAPI.setURL('https://git.yoursite.com')
       *
       * // from a .vue component
       * this.GitHubAPI.setURL('https://git.yoursite.com')
       */
      setURL: function (newURL) {
        if (typeof newURL === 'undefined' || newURL == null || newURL === '') {
          console.error('[vue-github-api] setURL requires non empty newURL')
          return
        }
        apiUrl = newURL
      },

      /**
       * A request callback function.
       *
       * @callback requestCallback
       * @param {Object} response Full response from GitHub API
       */

      /**
       * A request error callback function.
       *
       * @callback errorCallback
       * @param {Object} response Full response from GitHub API
       */

      /**
       * Issue a GET request on 'https://api.github.com' with params and a variable to fill in
       * @param  {String}                  uri     The GitHub API uri to consume such as '/user/repos'
       * @param  {Object}                  params  A parameters object such as { 'project_id': 72 }
       * @param  {(Array|requestCallback)} fillIn  The Vue.js defined data to fill in with results from GitHub API, or a callback function fed with full response
       * @param  {errorCallback}           errorCb A callback function in case of error (response is passed to callback)
       * @example
       * // -------------------------------------------------
       * // 1- With an array to fill in a Vue.js defined data
       * // -------------------------------------------------
       * // from anywhere in the global Vue scope
       * Vue.GitHubAPI.get('/user/repos', {}, [this.myGitHubData, 'repositories'])
       * // from a .vue component
       * this.GitHubAPI.get('/user/repos', {}, [this.myGitHubData, 'repositories'])
       *
       *
       * // ----------------------------------------------------------
       * // 2- With a callback function to play with the full response
       * // ----------------------------------------------------------
       * // from anywhere in the global Vue scope
       * Vue.GitHubAPI.get('/user/repos', {}, (response) => {
       *   console.log('got response:', response)
       * })
       * // from a .vue component
       * this.GitHubAPI.get('/user/repos', {}, (response) => {
       *   console.log('got response:', response)
       * })
       */
      get: function (uri, params, fillIn, errorCb) {
        // verifying what user sends to fill in
        if (this._verifyFillIn(fillIn) !== true) {
          return
        }

        // ensure leading slash on uri
        uri = uri.replace(/^\/?/, '/')

        // downloading starts now
        this._updateStore('downloading')

        // request it with headers an params
        Vue.http.get(
          apiUrl + uri,
          {
            headers: {
              'Authorization': 'token ' + token
            },
            params: params
          }
        ).then((response) => {
          // no more downloading
          this._updateStore('downloaded')

          if (typeof fillIn === 'function') {
            // sending back the full response
            fillIn(response)
          } else {
            // fill in the data from the response body
            Vue.set(fillIn[0], fillIn[1], response.body)
          }
        }, (response) => {
          // no more downloading
          this._updateStore('downloaded')

          if (typeof errorCb === 'function') {
            // user defined an error callback function, calling it with response
            errorCb(response)
          } else {
            // no errorCb function defined, default to console error
            console.error('[vue-github-api] GET ' + uri + ' failed: "' + response.body.message + '" on ' + apiUrl + ' (using token "' + token + '")')
          }
        })
      },

      /**
       * Issue a POST request on 'https://api.github.com' with params and a variable to fill in
       * @param  {String}                  uri     The GitHub API uri to consume such as '/user/repos'
       * @param  {Object}                  params  A parameters object such as { 'project_id': 72 }
       * @param  {(Array|requestCallback)} fillIn  The Vue.js defined data to fill in with results from GitHub API, or a callback function fed with full response
       * @param  {errorCallback}           errorCb A callback function in case of error (response is passed to callback)
       * @example
       * // from anywhere in the global Vue scope
       * Vue.GitHubAPI.post('/repos/OWNER_NAME/REPO_NAME/issues', {
       *   title: 'My new issues from vue-github-api'
       * }, (response) => { console.log('posted it!', response) })
       *
       * // from a .vue component
       * this.GitHubAPI.post('/repos/OWNER_NAME/REPO_NAME/issues', {
       *   title: 'My new issues from vue-github-api'
       * }, (response) => { console.log('posted it!', response) })
       */
      post: function (uri, params, fillIn, errorCb) {
        // verifying what user sends to fill in
        if (this._verifyFillIn(fillIn) !== true) {
          return
        }

        // ensure leading slash on uri
        uri = uri.replace(/^\/?/, '/')

        // downloading starts now
        this._updateStore('downloading')

        // request it with headers an params
        Vue.http.post(
          apiUrl + uri,
          params,
          {
            headers: {
              'Authorization': 'token ' + token
            }
          }
        ).then((response) => {
          // no more downloading
          this._updateStore('downloaded')

          if (typeof fillIn === 'function') {
            // sending back the full response
            fillIn(response)
          } else {
            // fill in the data from the response body
            Vue.set(fillIn[0], fillIn[1], response.body)
          }
        }, (response) => {
          // no more downloading
          this._updateStore('downloaded')

          if (typeof errorCb === 'function') {
            // user defined an error callback function, calling it with response
            errorCb(response)
          } else {
            // no errorCb function defined, default to console error
            console.error('[vue-github-api] POST ' + uri + ' failed: "' + response.body.message + '" on ' + apiUrl + ' (using token "' + token + '")')
          }
        })
      },

      /**
       * Register your application Vuex store to improve it with GitHubAPI Vuex store module
       * @param  {Object} store Your application Vuex store
       * @example
       * // from within a .vue component
       * this.GitHubAPI.registerStore(this.$store)
       */
      registerStore: function (store) {
        if (typeof store === 'undefined') {
          console.error('[vue-github-api] This do not appear to be a Vuex store')
          return
        }

        // registering GitHubAPI Vuex module
        store.registerModule('GitHubAPI', {
          state: {
            // is GitHubAPI currently downloading?
            downloading: false,
            // how many downloads are running
            running: 0
          },
          mutations: {
            downloading: function (state) {
              // currently downloading
              state.running++
              state.downloading = true
            },
            downloaded: function (state) {
              // stopped downloading
              state.running--
              if (state.running === 0) {
                state.downloading = false
              }
            }
          }
        })

        // will need to update it later
        this._vuexStore = store
      },

      /**
       * If attached, the application wide Vuex store
       * @type {Object}
       * @private
       */
      _vuexStore: null,

      /**
       * Update the Vuex store if any
       * @param  {String} mutation The mutation to commit (downloading or downloaded)
       * @private
       */
      _updateStore: function (mutation) {
        if (this._vuexStore != null) {
          this._vuexStore.commit(mutation)
        }
      },

      /**
       * Verifying what to fill in from API consuming functions
       * @param  {(Array|requestCallback)} fillIn  The Vue.js defined data to fill in with results from GitHub API, or a callback function fed with full response
       * @return {Boolean}                         True if the fillIn type is ok, false (with a console error) if not
       * @private
       */
      _verifyFillIn: function (fillIn) {
        // fillIn can be a callback function to which the response will be sent
        if (typeof fillIn !== 'function') {
          // variable to fill in MUST be defined, as a Vue data (to be reactive),
          // and user MUST provide the key to fill in within that data, examples:
          // - [this.dataPropertyName, 'keyToFillDataIn']
          // - [this.GitHub, 'repositories']
          if (!(fillIn instanceof Array) || fillIn.length < 2) {
            console.error('[vue-github-api] You MUST define the Vue data you want to fill as a two values array')
            return false
          }

          // ensure reactive data is not an array but an object, or Vue.set will
          // fail in indexing the data to the expected key
          if (Array.isArray(fillIn[0]) || typeof fillIn[0] !== 'object') {
            console.error('[vue-github-api] Your Vue data to fill MUST be an object (ie `{}`)')
            return false
          }
        }

        return true
      }
    }
  }

  if (typeof exports === 'object') {
    module.exports = install
  }
})()
