# vue-github-api

A deadly simple Vue.js plugin to consume GitHub API.

Head to [the extensive JSDoc API documentation for vue-github-api](https://clorichel.gitlab.io/vue-github-api/Vue_GitHubAPI.html), or keep on reading for an easy process to integrate it in your application.

## Using it

### Install and import

```bash
npm install vue-github-api
```

In your main application JS file (typically `main.js` if you are using [vue-cli](https://github.com/vuejs/vue-cli) webpack template), simply use the plugin:

```javascript
// vue-resource is needed too
import VueResource from 'vue-resource'
Vue.use(VueResource)

// import vue-github-api
import GitHubAPI from 'vue-github-api'
Vue.use(GitHubAPI, { token: 'user Personal Access Token' })
```

You can configure _application wide_ options while `Vue.use`'ing this plugin:

| Name     | Description                                                           |
|----------|-----------------------------------------------------------------------|
| `token`  | one of your GitHub user _Personal Access Token_ to connect to the API |
| `apiURL` | base URL of your GitHub enterprise service (optional)                 |

### Consume GitHub API

From anywhere now, you can simply consume GitHub API:

```javascript
Vue.GitHubAPI.get('/projects', {}, [this.myGitHubData, 'projects'])
```

You can also use it in your `.vue` components with `this.GitHubAPI`:

```javascript
this.GitHubAPI.get('/projects', {}, [this.myGitHubData, 'projects'])
```

That was it! Now that you are bootstrapped in, have a look at all the methods available to you in the extensive JSDoc API documentation available on [these auto-generated GitLab Pages](https://clorichel.gitlab.io/vue-github-api/Vue_GitHubAPI.html).

**Important:** if you want your filled-in property `this.myGitHubData.projects` to be reactive "the Vue.js way", you MUST define this variable as a data in your components or vues, with a default value of an empty object (read `myGitHubData: {}`). See how to do it on a vue component on this example:

```javascript
<template>
  <div>
    <p>Repositories grabbed: {{ repositoriesCount }}</p>
  </div>
</template>

<script>
export default {
  data () {
    return {
      myGitHubData: {}
    }
  },
  mounted: function () {
    this.GitHubAPI.get('/user/repos', {}, [this.myGitHubData, 'repositories'])
  },
  computed: {
    repositoriesCount: function () {
      if (this.myGitHubData.repositories) {
        return this.myGitHubData.repositories.length
      }
      return 'none yet...'
    }
  }
}
</script>
```

This is due to the fact Vue does not allow to add new reactive properties dynamically. Read more about it on the awesome [Vue.js guide](http://vuejs.org/guide/reactivity.html#Change-Detection-Caveats).

During your application workflow, you could want to change GitHub user authentication token. This method will let you do it easily:

```javascript
// set application wide GitHubAPI token value
this.GitHubAPI.setToken('other user Personal Access Token')
```

## Vuex store module

Using Vuex? You can attach _vue-github-api_ Vuex module to your application store. Within one of your `.vue` components, simply register it before you using it from your application:

```javascript
// registers GitHubAPI Vuex module to your application Vuex store
this.GitHubAPI.registerStore(this.$store)

// you are now able to read this state
this.$store.state.GitHubAPI.downloading
```

Here are Vuex `states` provided by _vue-github-api_ to your application:

| Vuex state    | Type    | Description                                        |
|---------------|---------|----------------------------------------------------|
| `downloading` | Boolean | Defines if vue-github-api is currently downloading |
| `running`     | Number  | Count vue-github-api requests running now          |

Have a look at [Vuex](https://github.com/vuejs/vuex) for more details, or read on this complete example:

```javascript
import Vue from 'vue'

// your application is using Vuex
import Vuex from 'vuex'
Vue.use(Vuex)

// remember vue-resource is needed for vue-github-api
import VueResource from 'vue-resource'
Vue.use(VueResource)

// using vue-github-api
import GitHubAPI from 'vue-github-api'
Vue.use(GitHubAPI, { token: 'user Private Token' })

// declare your Vuex store
const store = new Vuex.Store({})

// this is your application
const app = new Vue({
  el: '#app',
  mounted: {
    // register GitHubAPI Vuex store!
    this.GitHubAPI.registerStore(store)
  },
  computed: {
    // with this computed property, do something insanely simple such as:
    // <p v-if="downloading">Downloading from GitHub...</p>
    downloading: function () {
      if (typeof store.state.GitHubAPI !== 'undefined') {
        return store.state.GitHubAPI.downloading
      } else {
        return false
      }
    }
  }
})
```

## Contributing

Initial scaffolding was done with [vue-cli](https://github.com/vuejs/vue-cli) webpack template. For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

Simply said, one can start working on its own customized version of _vue-github-api_ in no time:

``` bash
# install dependencies
npm install

# run unit tests
npm run unit

# build JSDoc API documentation (defaults to the ./out folder)
./node_modules/.bin/jsdoc src/GitHubAPI.js

# serve with hot reload at localhost:8080
npm run dev
```

## What's next?

Without any obligation nor due date, one could expect to be done this non-exhaustive [list of improvements grouped on issues labeled `Feature`](https://gitlab.com/clorichel/vue-github-api/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=Feature).

You can read on the [Changelog](CHANGELOG.md) too, for historical and upcoming new features, changes, deprecations, removed features, bug and security fixes.

## Support

Your are free to [open an issue](https://gitlab.com/clorichel/vue-github-api/issues/new) right in this GitLab repository whether you should be facing a problem or a bug. Please advise this is not a commercial product, so one could experience random response time. Positive, friendly and productive conversations are expected on the issues. Screenshots and steps to reproduce are highly appreciated. Chances are you may get your issue solved if you follow these simple guidelines.


## Credits

- Logo derived from a [SlideGenius](https://thenounproject.com/slidegenius/) creation [CC licensed](https://creativecommons.org/licenses/by/3.0/us/)

## License

The _vue-github-api_ plugin is distributed under the [MIT License (MIT)](LICENSE). Please have a look at the dependencies licenses if you plan on using, building, or distributing this plugin.